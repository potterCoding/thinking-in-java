package annotations;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Mr.Sun
 * @date 2022年09月03日 9:11
 *
 * 编写注解处理器
 */
public class UseCaseTracker {

    public static void trackUseCases(List<Integer> useCases, Class<?> clazz) {
        for (Method m : clazz.getDeclaredMethods()) {
            UseCase useCase = m.getAnnotation(UseCase.class);
            if (useCase != null) {
                System.out.println("找到@UseCase：" + useCase.id() + " "  + useCase.description());
                useCases.remove(Integer.valueOf(useCase.id()));
            }
        }
        for (Integer i : useCases) {
            System.out.println("警告: 缺失用例：" + i);
        }
    }

    public static void main(String[] args) {
        List<Integer> useCases = new ArrayList<>();
        Collections.addAll(useCases, 47, 48, 49, 50);
        trackUseCases(useCases, PasswordUtils.class);
    }
} /* Output:
找到@UseCase：49 新密码不能等于以前使用的密码
找到@UseCase：47 密码必须至少包含一个数字
找到@UseCase：48 no description
警告: 缺失用例：50
*///:~
