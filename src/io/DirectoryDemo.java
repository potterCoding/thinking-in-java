package io;

import java.io.File;

/**
 * @author Mr.Sun
 * @date 2022年08月30日 21:06
 */
public class DirectoryDemo {

    public static void main(String[] args) {
        // 所有的目录
        PPrint.pprint(Directory.walk(".").dirs);
        // 所有以T开始的文件
        for (File file : Directory.local(".", "T.*")) {
            System.out.println(file);
        }
        System.out.println("-----------------------------");
        // 所有以T开头的Java文件
        for (File file : Directory.walk(".", "T.*\\.java")) {
            System.out.println(file);
        }
        System.out.println("==============================");
        // Class文件包含Z或z的
        for (File file : Directory.walk(".", ".*[Zz].*\\.class")) {
            System.out.println(file);
        }
    }
}
