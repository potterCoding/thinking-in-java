package io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author Mr.Sun
 * @date 2022年09月02日 10:24
 *
 * 读取二进制文件
 */
public class BinaryFile {

    public static byte[] read(File bfile) throws IOException {
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(bfile));
        try {
            byte[] data = new byte[in.available()];
            in.read(data);
            return data;
        } finally {
            in.close();
        }
    }

    public static byte[] read(String bfile) throws IOException {
        return read(new File(bfile).getAbsoluteFile());
    }
}
