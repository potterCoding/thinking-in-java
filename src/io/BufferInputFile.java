package io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Mr.Sun
 * @date 2022年08月31日 20:53
 *
 * 缓冲输入文件
 */
public class BufferInputFile {

    /**
     * 读取文件
     */
    public static String read(String filename) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(filename));
        String s;
        // 字符串sb用来累计文件的全部内容（包括必须添加的换行符，因为readline已将它们删掉）
        StringBuilder sb = new StringBuilder();
        while ((s = in.readLine()) != null) {
            sb.append(s + "\n");
        }
        // 最后，调用close()关闭文件
        in.close();
        return sb.toString();
    }

    public static void main(String[] args) throws IOException {
        System.out.println(read("src/io/BufferInputFile.java"));
    }
}
