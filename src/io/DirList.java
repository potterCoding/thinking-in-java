package io;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * @author Mr.Sun
 * @date 2022年08月30日 20:16
 *
 * 使用”目录过滤器“，显示符合条件的File对象
 */
public class DirList {

    public static void main(String[] args) {
        File path = new File("G:\\demo");
        String[] list;
        if (args.length == 0) {
            list = path.list();
        } else {
            list = path.list(new DirFilter(args[0]));
        }
        // 按照字母排序
        Arrays.sort(list, String.CASE_INSENSITIVE_ORDER);
        for (String dirItem : list) {
            System.out.println(dirItem);
        }
    }
}

class DirFilter implements FilenameFilter {

    private Pattern pattern;

    public DirFilter(String regex) {
        this.pattern = Pattern.compile(regex);
    }

    @Override
    public boolean accept(File dir, String name) {
        return pattern.matcher(name).matches();
    }
}
/* Output:
DirectoryDemo.java
DirList.java
DirList2.java
DirList3.java
 *///:~
