package io;

import java.io.IOException;
import java.io.StringReader;

/**
 * @author Mr.Sun
 * @date 2022年08月31日 21:04
 *
 * 从内存输入
 */
public class MemoryInput {
    public static void main(String[] args) throws IOException {
        StringReader in = new StringReader(BufferInputFile.read("src/io/MemoryInput.java"));
        int c;
        // 注意read()方法是以int形式返回下一个字节，因此必须类型转换为char才能正确打印
        while ((c = in.read()) != -1) {
            System.out.print((char)c);
        }
        in.close();
    }
}
