package utils;

import generics.tuple.FiveTuple;
import generics.tuple.FourTuple;
import generics.tuple.ThreeTuple;
import generics.tuple.TwoTuple;

/**
 * @author Mr.Sun
 * @date 2022年02月23日 20:18
 *
 * 类型推断+static方法，封装元组工具
 */
public class Tuple {

    public static <A,B> TwoTuple tuple(A a, B b) {
        return new TwoTuple<>(a, b);
    }

    public static <A,B,C> ThreeTuple tuple(A a, B b, C c) {
        return new ThreeTuple<>(a, b, c);
    }

    public static <A,B,C,D> FourTuple tuple(A a, B b, C c, D d) {
        return new FourTuple<>(a, b, c, d);
    }

    public static <A,B,C,D,E> FiveTuple tuple(A a, B b, C c, D d, E e) {
        return new FiveTuple<>(a, b, c, d, e);
    }
}
