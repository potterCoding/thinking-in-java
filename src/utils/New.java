package utils;

import java.util.*;

/**
 * @author Mr.Sun
 * @date 2022年02月22日 21:03
 *
 * 类型参数推断演示
 */
public class New {

    public static <K,V> Map<K,V> map() {
        return new HashMap<>();
    }

    public static <T> List<T> list() {
        return new ArrayList<>();
    }

    public static <T> LinkedList<T> linkedList() {
        return new LinkedList<>();
    }

    public static <T> Set<T> set() {
        return new HashSet<>();
    }

    public static <T> Queue<T> queue() {
        return new LinkedList<>();
    }

    public static void main(String[] args) {
        Map<String, List<String>> map = New.map();
        List<String> list = New.list();
        LinkedList<String> ls = New.linkedList();
        Set<String> set = New.set();
        Queue<String> queue = New.queue();
    }
}
