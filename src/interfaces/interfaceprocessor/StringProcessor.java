package interfaces.interfaceprocessor;

import java.util.Arrays;

/**
 * @author Mr.Sun
 * @date 2022年01月21日 11:00
 *
 * 复用代码的第一种方式是客户端程序员遵循改接口来编写他们自己的类
 */
public abstract class StringProcessor implements Processor {
    @Override
    public String name() {
        return getClass().getSimpleName();
    }

    public abstract Object process(Object input);

    public static String s = "If she weighs the same as a duck, she's made of wood";

    public static void main(String[] args) {
        Apply.process(new UpCase(), s);
        Apply.process(new DownCase(), s);
        Apply.process(new Splitter(), s);
    }
}

class UpCase extends StringProcessor {
    @Override
    public Object process(Object input) {
        return ((String)input).toUpperCase();
    }
}

class DownCase extends StringProcessor {
    @Override
    public Object process(Object input) {
        return ((String)input).toLowerCase();
    }
}

class Splitter extends StringProcessor {
    @Override
    public Object process(Object input) {
        return Arrays.toString(((String)input).split(" "));
    }
}
