package interfaces.interfaceprocessor;

/**
 * @author Mr.Sun
 * @date 2022年01月21日 10:58
 */
public class Apply {

    public static void process(Processor p, Object s) {
        System.out.println("Using Processor " + p.name());
        System.out.println(p.process(s));
    }
}
