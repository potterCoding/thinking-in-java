package interfaces;

/**
 * @author Mr.Sun
 * @date 2022年01月21日 11:24
 *
 * 使用接口的核心原因：
 *  1.为了能够向上转型为多个基类型(以及由此而带来的灵活性)
 *  2.防止客户端程序员创建该类的对象，并确保这仅仅是建立一个接口
 */
interface CanFight {
    void fight();
}

interface CanSwim {
    void swim();
}

interface CanFly {
    void fly();
}

class ActionCharacter {
    public void fight() {}
}

/**
 * 当通过这种方式将一个具体类和多个接口组合在一起时，这个具体类必须放在前面，
 * 后面跟着的才是接口（否则编译器会报错）
 */
class Hero extends ActionCharacter
        implements CanFight, CanFly, CanSwim {

    @Override
    public void swim() { }

    @Override
    public void fly() { }
}

public class Adventure {
    public static void t(CanFight x) { x.fight(); }
    public static void f(CanFly x) { x.fly(); }
    public static void s(CanSwim x) { x.swim(); }
    public static void a(ActionCharacter x) { x.fight(); }

    public static void main(String[] args) {
        Hero h = new Hero();
        t(h);
        f(h);
        s(h);
        a(h);
    }
}
