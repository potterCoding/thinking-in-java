package interfaces.filters;

/**
 * @author Mr.Sun
 * @date 2022年01月21日 10:50
 */
public class Filter {

    public String name() {
        return getClass().getSimpleName();
    }
    public Waveform process(Waveform input) {
        return input;
    }
}
