package interfaces.filters;

/**
 * @author Mr.Sun
 * @date 2022年01月21日 10:53
 */
public class HighPass extends Filter {

    double cutoff;

    public HighPass(double cutoff) {
        this.cutoff = cutoff;
    }

    @Override
    public Waveform process(Waveform input) {
        return input;
    }
}
