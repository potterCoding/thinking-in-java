package interfaces.filters;

/**
 * @author Mr.Sun
 * @date 2022年01月21日 10:47
 *
 * 假设我们发现了一组电子滤波器，它看起来好像适用于Apply.process()方法
 */
public class Waveform {
    private static long counter;
    private final long id = counter++;

    @Override
    public String toString () {
        return "Waveform " + id;
    }

}
