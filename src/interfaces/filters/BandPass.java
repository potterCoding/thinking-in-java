package interfaces.filters;

/**
 * @author Mr.Sun
 * @date 2022年01月21日 10:53
 */
public class BandPass extends Filter {

    double lowCutOff, highCutOff;

    public BandPass(double lowCutOff, double highCutOff) {
        this.lowCutOff = lowCutOff;
        this.highCutOff = highCutOff;
    }

    @Override
    public Waveform process(Waveform input) {
        return input;
    }
}
