package interfaces.filters;

/**
 * @author Mr.Sun
 * @date 2022年01月21日 10:51
 */
public class LowPass extends Filter {

    double cutoff;

    public LowPass(double cutoff) {
        this.cutoff = cutoff;
    }

    @Override
    public Waveform process(Waveform input) {
        return input;
    }
}
