package array;

import utils.Generator;

/**
 * @author Mr.Sun
 * @date 2022年08月26日 21:53
 */
public class GeneratorTest {
    public static int size = 10;
    public static void test(Class<?> clazz) {
        // 通过反射方法clazz.getClasses()可以获取到所有的嵌套类
        for (Class<?> type : clazz.getClasses()) {
            System.out.println(type.getSimpleName());
            try {
                // 每个Generator都有一个无参构造器
                Generator<?> gen = (Generator<?>) type.newInstance();
                for (int i = 0; i < size; i++) {
                    System.out.print(gen.next() + " ");
                }
                System.out.println();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
//        test(CountingGenerator.class);
        test(RandomGenerator.class);
    }
}
