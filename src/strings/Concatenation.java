package strings;

/**
 * @author Mr.Sun
 * @date 2022年04月10日 10:17
 *
 * 重载“+”测试
 */
public class Concatenation {
    public static void main(String[] args) {
        String mango = "mango";
        String s = "abc" + mango + "def" + 47;
        System.out.println(s);
    }
}
