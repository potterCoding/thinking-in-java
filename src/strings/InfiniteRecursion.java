package strings;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mr.Sun
 * @date 2022年02月16日 20:31
 *
 * 无意识的递归
 */
public class InfiniteRecursion {
    public static void main(String[] args) {
        List<InfiniteRecursion> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            InfiniteRecursion v = new InfiniteRecursion();
            list.add(v);
        }
        System.out.println(list);
    }

    @Override
    public String toString() {
//        return "InfiniteRecursion address: " + this +"\n";
        return super.toString();
    }
}
