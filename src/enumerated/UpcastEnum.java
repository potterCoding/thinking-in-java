package enumerated;

/**
 * @author Mr.Sun
 * @date 2022年09月02日 16:35
 */
enum Search {
    HITHER, YON
}


public class UpcastEnum {
    public static void main(String[] args) {
        Search[] values = Search.values();
        for (Search val : values) {
            System.out.println(val.name());
        }
        System.out.println("--------------");
        Enum e = Search.HITHER;
        for (Enum en : e.getClass().getEnumConstants()) {
            System.out.println(en);
        }
    }
}
