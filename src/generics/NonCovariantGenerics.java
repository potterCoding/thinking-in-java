package generics;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mr.Sun
 * @date 2022年02月24日 20:55
 */
public class NonCovariantGenerics {
    // Compile Error: incompatible types:
//    List<Fruit> flist = new ArrayList<Apple>();
}
