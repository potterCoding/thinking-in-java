package generics.coffee;

/**
 * @author Mr.Sun
 * @date 2022年02月21日 21:36
 *
 * 演示使用泛型接口实现生成器
 * 生成器是一种专门负责创建对象的类，生成器无需额外的信息就知道如何产生新的对象
 */
public class Coffee {
    private static long counter = 0;

    private final long id = counter++;

    public String toString() {
        return getClass().getSimpleName() + " " + id;
    }
}
