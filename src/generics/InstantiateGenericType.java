package generics;

/**
 * @author Mr.Sun
 * @date 2022年02月23日 21:42
 *
 * 无法在Java中实现new T()的部分原因是因为擦除，而另一部分原因是因为编译器
 * 不能验证T具有无参构造器
 */
class ClassAsFactory<T> {
    T x;
    public ClassAsFactory(Class<T> kind) {
        try {
            x = kind.newInstance();
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}

class Employee {}

public class InstantiateGenericType {
    public static void main(String[] args) {
        ClassAsFactory<Employee> fe = new ClassAsFactory<>(Employee.class);
        System.out.println("ClassAsFactory<Employee> succeeded");

        try {
            // 因为Integer没有任何默认的构造器
            ClassAsFactory<Integer> fi = new ClassAsFactory<>(Integer.class);
        } catch(Exception e) {
            System.out.println("ClassAsFactory<Integer> failed");
        }
    }
}
