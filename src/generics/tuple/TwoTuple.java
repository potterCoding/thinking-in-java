package generics.tuple;

/**
 * @author Mr.Sun
 * @date 2022年02月21日 20:29
 *
 * 元组：它是将一组对象直接打包存储于其中的一个单一对象。
 * 这个容器对象允许读取其中元素，但是不允许向其中存放新的对象（这个概念也称数据传输对象或信使）
 *
 * 通常，元组可以具有任意长度，同时，元组中的对象可以是任意不同类型。
 * 二维元组：它能够持有两个对象
 */
public class TwoTuple<A, B> {
    public final A first;

    public final B second;

    public TwoTuple(A a, B b) {
        first = a;
        second = b;
    }
    // 注意：元组隐含地保持了其中元素的次序
    public String toString() {
        return "(" + first + ", " + second + ")";
    }
}
