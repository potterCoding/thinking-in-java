package generics.tuple;

import java.util.ArrayList;

/**
 * @author Mr.Sun
 * @date 2022年02月23日 20:48
 *
 * 构建复杂模型
 */
public class TupleList <A,B,C,D> extends ArrayList<FourTuple<A,B,C,D>> {

    public static void main(String[] args) {
        TupleList<Vehicle, Amphibian, String, Integer> tl = new TupleList<>();
        tl.add(TupleTest.h());
        tl.add(TupleTest.h());
        for(FourTuple<Vehicle,Amphibian,String,Integer> i: tl)
            System.out.println(i);
    }
}
