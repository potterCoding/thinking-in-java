package generics;

import java.util.List;

/**
 * @author Mr.Sun
 * @date 2022年02月24日 21:15
 *
 * 超类型通配符
 */
public class SuperTypeWildcards {
    static void writeTo(List<? super Apple> apples) {
        apples.add(new Apple());
        apples.add(new Jonathan());
        // apples.add(new Fruit()); // Error
    }
}
