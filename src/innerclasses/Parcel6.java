package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月22日 11:01
 *
 * 在方法和作用域内的内部类
 */
public class Parcel6 {

    public Destination destination(String s) {
        class PDestination implements Destination {
            private String label;
            private PDestination(String whereTo) {
                label = whereTo;
            }
            @Override
            public String readLabel() {
                return label;
            }
        }
        return new PDestination(s);
    }

    public static void main(String[] args) {
        Parcel6 parcel6 = new Parcel6();
        Destination d = parcel6.destination("Tasmania");
    }
}
