package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月22日 10:51
 *
 * 内部类与向上转型
 */
public class TestParcel {
    public static void main(String[] args) {
        Parcel5 p = new Parcel5();
        Contents contents = p.contents();
        Destination d = p.destination("Tasmania");
        // 不能访问私有内部类
        //Parcel5.PContents = p.new PContents();
    }
}

class Parcel5 {
    private class PContents implements Contents {
        private int i = 11;
        @Override
        public int value() {
            return i;
        }
    }
    protected class PDestination implements Destination {
        private String label;

        public PDestination(String whereTo) {
            this.label = whereTo;
        }

        @Override
        public String readLabel() {
            return label;
        }
    }

    public Destination destination(String s) {
        return new PDestination(s);
    }

    public Contents contents() {
        return new PContents();
    }
}
