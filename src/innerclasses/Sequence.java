package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月22日 10:02
 *
 * 当生成一个内部类的对象时，此对象与制造它的外围对象之间就有了一种联系，
 * 所以它能访问其外围对象的所有成员，而不需要任何特殊条件。
 * 此外，内部类还拥有其外围类的所有元素的访问权
 */
interface Selector {
    // 检查元素是否到末尾
    boolean end();
    // 访问当前对象
    Object current();
    // 移动到序列中的下一个对象
    void next();
}

public class Sequence {
    private Object[] items;
    private int next = 0;

    public Sequence(int size) {
        this.items = new Object[size];
    }

    public void add(Object o) {
        if (next < items.length) {
            items[next++] = o;
        }
    }

    // 内部类可以访问外围类的方法和字段
    private class SequenceSelector implements Selector {
        private int i = 0;

        @Override
        public boolean end() {
            // 内部类自动拥有对其外围类所有成员的访问权
            return i == items.length;
        }

        @Override
        public Object current() {
            return items[i];
        }

        @Override
        public void next() {
            if (i < items.length) {
                i++;
            }
        }
    }

    public Selector selector() {
        return new SequenceSelector();
    }

    public static void main(String[] args) {
        Sequence sequence = new Sequence(10);
        for (int i = 0; i < 10; i++) {
            sequence.add(Integer.toString(i));
        }
        Selector selector = sequence.selector();
        while (!selector.end()) {
            System.out.print(selector.current() + " ");
            selector.next();
        }
    }
}
