package innerclasses.controller;

/**
 * @author Mr.Sun
 * @date 2022年01月23日 11:52
 *
 * 创建一个GreenhouseControls对象，并添加各种不同的Event对象来配置该系统
 * 这是命令设计模式的一个例子
 */
public class GreenhouseController {
    public static void main(String[] args) {
        GreenhouseControls controls = new GreenhouseControls();
        controls.addEvent(controls.new Bell(1000));
        Event[] eventList = {
                controls.new ThermostatNight(0),
                controls.new LightOn(200),
                controls.new LightOff(400),
                controls.new WaterOn(600),
                controls.new WaterOff(800),
                controls.new ThermostatDay(1400)
        };
        controls.addEvent(controls.new Restart(2000, eventList));
        if (args.length == 1) {
            controls.addEvent(
                    new GreenhouseControls.Terminate(
                            new Integer(args[0])));
        }
        controls.run();
    }
}
