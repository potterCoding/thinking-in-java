package innerclasses.controller;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mr.Sun
 * @date 2022年01月23日 11:25
 */
public class Controller {
    private List<Event> eventList = new ArrayList<>();
    public void addEvent(Event e) {
        eventList.add(e);
    }

    public void run() {
        while (eventList.size() > 0) {
            // 遍历eventList，寻找就绪的、要运行的Event对象
            // 找到的每一个就绪的事件，使用对象的toString()打印其信息
            // 调用其action()
            // 然后从队列中移除
            for (Event event : new ArrayList<>(eventList)) {
                System.out.println(event);
                event.action();
                eventList.remove(event);
            }
        }
    }

}
