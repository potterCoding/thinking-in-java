package innerclasses.controller;

/**
 * @author Mr.Sun
 * @date 2022年01月23日 11:21
 *
 * 控制事件
 * 使变化的事物与不变的事务相互分离
 */
public abstract class Event {
    private long eventTime;
    protected final long delayTime;
    public Event(long delayTime) {
        this.delayTime = delayTime;
        start();
    }

    public void start() {
        eventTime = System.nanoTime() + delayTime;
    }

    public boolean ready() {
        return System.nanoTime() >= eventTime;
    }
    // 如果想要重复一个事件，只需要简单地在action()中调用start()
    public abstract void action();
}
