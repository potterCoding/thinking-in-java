package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月22日 11:06
 */
public class Parcel7 {
    private void internalTracking(boolean b) {
        if (b) {
            class TrackingSlip {
                private String id;
                TrackingSlip(String s) {
                    id = s;
                }
                String getSlip() {
                    return id;
                }
            }
            TrackingSlip ts = new TrackingSlip("slip");
            String s = ts.getSlip();
            System.out.println(s);
        }
        // 不能在这里使用，因为已经超出作用域
//        TrackingSlip ts = new TrackingSlip("slip");
    }
    public void track()  {internalTracking(true);}

    public static void main(String[] args) {
        Parcel7 p = new Parcel7();
        p.track();
    }
}
