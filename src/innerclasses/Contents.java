package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月22日 10:50
 */
public interface Contents {
    int value();
}
