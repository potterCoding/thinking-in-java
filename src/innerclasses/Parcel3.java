package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月22日 10:41
 */
public class Parcel3 {

   static class Contents {
        private int i = 11;
        public int value() {
            return i;
        }
    }

    public static void main(String[] args) {
        Parcel3.Contents contents = new Parcel3.Contents();
        System.out.println(contents.value());
    }

}
