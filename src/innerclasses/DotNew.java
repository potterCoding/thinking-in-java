package innerclasses;

/**
 * @author Mr.Sun
 * @date 2022年01月22日 10:21
 *
 * 有时候你可能想要告知某些其他对象，去创建某个内部类的对象。这时你需要使用
 * .new语法
 */
public class DotNew {

    public class Inner {}

    public static void main(String[] args) {
        DotNew dotNew = new DotNew();
        Inner inner = dotNew.new Inner();
    }
}
