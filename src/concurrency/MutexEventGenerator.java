package concurrency;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Mr.Sun
 * @date 2022年09月04日 11:13
 *
 * 使用显示的Lock对象重写SyncEventGenerator
 */
public class MutexEventGenerator extends IntGenerator{
    private int currentCountVal = 0;
    private Lock lock = new ReentrantLock();

    @Override
    public int next() {
        lock.lock();
        try {
            ++currentCountVal;
            Thread.yield();
            ++currentCountVal;
            return currentCountVal;
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        EventChecker.test(new MutexEventGenerator());
    }
}
