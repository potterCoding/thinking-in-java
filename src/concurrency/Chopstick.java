package concurrency;

/**
 * @author Mr.Sun
 * @date 2022年09月06日 11:07
 */
public class Chopstick {

    // 筷子是否被拿走
    private boolean taken = false;

    // 拿筷子
    public synchronized void take() throws InterruptedException {
        while (taken) {
            wait();
        }
        taken = true;
    }

    // 归坏筷子
    public synchronized void drop() {
        taken = false;
        notifyAll();
    }
}
