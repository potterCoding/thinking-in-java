package concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Mr.Sun
 * @date 2022年09月03日 17:02
 *
 * 捕获线程异常
 */
public class NativeExceptionHandling {

    public static void main(String[] args) {
        ExecutorService exec = Executors.newCachedThreadPool();
        try {
            exec.execute(new ExceptionThread());
        } catch (RuntimeException e) {
            // 该语句将不会被执行
            System.out.println("异常被捕获处理");
        } finally {
            exec.shutdown();
        }
    }
}
