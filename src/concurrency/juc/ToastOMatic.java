package concurrency.juc;

import container.Stack;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author Mr.Sun
 * @date 2022年09月06日 9:53
 *
 * 制作吐司 -> 涂黄油 -> 涂果酱 -> 咀嚼消费
 */
public class ToastOMatic {

    public static void main(String[] args) throws Exception {
        ToastQueue dryQueue = new ToastQueue(),
                butterQueue = new ToastQueue(),
                finishedQueue = new ToastQueue();

        ExecutorService exec = Executors.newCachedThreadPool();
        // 制作吐司
        exec.execute(new Toaster(dryQueue));
        // 为制作好的吐司涂上黄油
        exec.execute(new Butterer(dryQueue, butterQueue));
        // 为涂上黄油的吐司涂抹果酱
        exec.execute(new Jammer(butterQueue, finishedQueue));
        // 将涂抹好果酱的吐司给用户咀嚼消费
        exec.execute(new Eater(finishedQueue));
        TimeUnit.SECONDS.sleep(5);
        exec.shutdownNow();
    }
}

class Toast {
    public enum Status {DRY, BUFFERED, JAMMED}

    private Status status = Status.DRY;
    private final int id;

    public Toast(int id) {
        this.id = id;
    }

    // 涂抹黄油
    public void butter() {
        status = Status.BUFFERED;
    }

    // 涂果酱
    public void jam() {
        status = Status.JAMMED;
    }

    public Status getStatus() {
        return status;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Toast " + id + ": " + status;
    }
}

class ToastQueue extends LinkedBlockingQueue<Toast> { }

class Toaster implements Runnable {
    private final ToastQueue toastQueue;
    private int count = 0;
    private final Random random = new Random(47);

    public Toaster(ToastQueue toastQueue) {
        this.toastQueue = toastQueue;
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                TimeUnit.MILLISECONDS.sleep(100 + random.nextInt(500));
                // 制作吐司
                Toast t = new Toast(count++);
                System.out.println(t);
                // 插入吐司队列
                toastQueue.put(t);
            }
        } catch (InterruptedException e) {
            System.out.println("Toaster Interrupted");
        }
        System.out.println("Toaster off");
    }
}

// 给吐司涂黄油
class Butterer implements Runnable {
    private final ToastQueue dryQueue, butteredQueue;

    public Butterer(ToastQueue dry, ToastQueue buttered) {
        dryQueue = dry;
        butteredQueue = buttered;
    }

    @Override
    public void run() {
        try {
            while(!Thread.interrupted()) {
                //  若刚制作的吐司队列为空，则阻塞等待
                Toast t = dryQueue.take();
                // 不为空，则为该吐司涂上黄油
                t.butter();
                System.out.println(t);
                // 将涂好黄油的吐司放进黄油吐司队列中
                butteredQueue.put(t);
            }
        } catch(InterruptedException e) {
            System.out.println("Butterer interrupted");
        }
        System.out.println("Butterer off");
    }
}

// 给涂黄油过后的吐司涂上果酱
class Jammer implements Runnable {
    private final ToastQueue butteredQueue, finishedQueue;

    public Jammer(ToastQueue buttered, ToastQueue finished) {
        butteredQueue = buttered;
        finishedQueue = finished;
    }

    public void run() {
        try {
            while(!Thread.interrupted()) {
                // 若已经涂黄油的吐司队列为空，则阻塞等待
                Toast t = butteredQueue.take();
                // 获取到涂抹黄油的吐司后，在涂上果酱
                t.jam();
                System.out.println(t);
                // 放入已制作完成的土司队列 中
                finishedQueue.put(t);
            }
        } catch(InterruptedException e) {
            System.out.println("Jammer interrupted");
        }
        System.out.println("Jammer off");
    }
}

// 消费吐司
class Eater implements Runnable {
    private final ToastQueue finishedQueue;
    private int counter = 0;

    public Eater(ToastQueue finished) {
        finishedQueue = finished;
    }

    @Override
    public void run() {
        try {
            while(!Thread.interrupted()) {
                // 若已完成的吐司队列为空，则阻塞等待
                Toast t = finishedQueue.take();
                // 验证吐司是否按顺序进行，所有吐司是否已经加了果酱
                if(t.getId() != counter++ || t.getStatus() != Toast.Status.JAMMED) {
                    System.out.println(">>>> Error: " + t);
                    System.exit(1);
                } else
                    System.out.println("咀嚼! " + t);
            }
        } catch(InterruptedException e) {
            System.out.println("Eater interrupted");
        }
        System.out.println("Eater off");
    }
}
