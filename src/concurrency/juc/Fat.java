package concurrency.juc;

/**
 * @author Mr.Sun
 * @date 2022年09月06日 17:22
 */
public class Fat {
    private volatile double d; // 防止优化
    private static int counter = 0;
    private final int id = counter++;

    public Fat() {
        // 耗时，可中断的操作
        for(int i = 1; i < 10000; i++) {
            d += (Math.PI + Math.E) / (double)i;
        }
    }

    public void operation() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "Fat id: " + id;
    }
}
