package concurrency;

/**
 * @author Mr.Sun
 * @date 2022年09月03日 22:02
 */
public class EventGenerator extends IntGenerator {

    private int currentEventVal = 0;

    @Override
    public int next() {
        ++currentEventVal;
        Thread.yield();
        // 可能出现资源竞争问题的地方
        ++currentEventVal;
        return currentEventVal;
    }

    public static void main(String[] args) {
        EventChecker.test(new EventGenerator());
    }
}
