package concurrency;

/**
 * @author Mr.Sun
 * @date 2022年09月03日 10:29
 *
 * 将Runnable对象转变为工作任务的传统方式是把它交给一个Thread构造器
 */
public class BasicThreads {

    public static void main(String[] args) {
        Thread t = new Thread(new LiftOff());
        t.start();
        System.out.println("Waiting for LiftOff");
    }
} /* Output:
Waiting for LiftOff
#0(9). #0(8). #0(7). #0(6). #0(5). #0(4). #0(3). #0(2). #0(1). #0(LiftOff!).
*///:~
