package concurrency;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author Mr.Sun
 * @date 2022年09月05日 16:00
 *
 * notifyAll()并非唤醒所有“正在等待”的任务，而是只会唤醒等待 调用notifyAll()某个特定锁的任务
 */
public class NotifyVsNotifyAll {

    public static void main(String[] args) throws Exception {
        ExecutorService exec = Executors.newCachedThreadPool();
        for (int i = 0; i < 5; i++) {
            exec.execute(new Task());
        }
        exec.execute(new Task2());
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            boolean prod = true;
            @Override
            public void run() {
                if (prod) {
                    System.out.print("\nnotify() ");
                    Task.blocker.prod();
                    prod = false;
                } else {
                    System.out.print("\nnotifyAll() ");
                    Task.blocker.prodAll();
                    prod = true;
                }
            }
        }, 400, 400);
        TimeUnit.SECONDS.sleep(5);

        timer.cancel();
        System.out.println("\nTimer canceled");
        TimeUnit.MILLISECONDS.sleep(500);

        System.out.print("Task2.blocker.prodAll() ");
        Task2.blocker.prodAll();

        TimeUnit.MILLISECONDS.sleep(500);
        System.out.println("\nShutting down");
        exec.shutdownNow(); // 中断所有任务
    }
}

class Blocker {

    synchronized void waitingCall() {
        try {
            while (!Thread.interrupted()) {
                wait();
                System.out.println(Thread.currentThread() + " ");
            }
        } catch (InterruptedException e) {

        }
    }

    synchronized void prod() {
        notify();
    }

    synchronized void prodAll() {
        notifyAll();
    }
}

class Task implements Runnable {

    static Blocker blocker = new Blocker();

    @Override
    public void run() {
        blocker.waitingCall();
    }
}

class Task2 implements Runnable {
    // 一个单独的blocker对象
    static Blocker blocker = new Blocker();

    @Override
    public void run() {
        blocker.waitingCall();
    }
} /* Output:

 notify() Thread[pool-1-thread-1,5,main]

 notifyAll() Thread[pool-1-thread-1,5,main]
 Thread[pool-1-thread-5,5,main]
 Thread[pool-1-thread-4,5,main]
 Thread[pool-1-thread-3,5,main]
 Thread[pool-1-thread-2,5,main]

 notify() Thread[pool-1-thread-1,5,main]

 notifyAll() Thread[pool-1-thread-1,5,main]
 Thread[pool-1-thread-2,5,main]
 Thread[pool-1-thread-3,5,main]
 Thread[pool-1-thread-4,5,main]
 Thread[pool-1-thread-5,5,main]

 notify() Thread[pool-1-thread-1,5,main]

 notifyAll() Thread[pool-1-thread-1,5,main]
 Thread[pool-1-thread-5,5,main]
 Thread[pool-1-thread-4,5,main]
 Thread[pool-1-thread-3,5,main]
 Thread[pool-1-thread-2,5,main]

 notify() Thread[pool-1-thread-1,5,main]

 notifyAll() Thread[pool-1-thread-1,5,main]
 Thread[pool-1-thread-2,5,main]
 Thread[pool-1-thread-3,5,main]
 Thread[pool-1-thread-4,5,main]
 Thread[pool-1-thread-5,5,main]

 notify() Thread[pool-1-thread-1,5,main]

 notifyAll() Thread[pool-1-thread-1,5,main]
 Thread[pool-1-thread-5,5,main]
 Thread[pool-1-thread-4,5,main]
 Thread[pool-1-thread-3,5,main]
 Thread[pool-1-thread-2,5,main]

 notify() Thread[pool-1-thread-1,5,main]

 notifyAll() Thread[pool-1-thread-1,5,main]
 Thread[pool-1-thread-2,5,main]
 Thread[pool-1-thread-3,5,main]
 Thread[pool-1-thread-4,5,main]
 Thread[pool-1-thread-5,5,main]

 Timer canceled
 Task2.blocker.prodAll() Thread[pool-1-thread-6,5,main]

 Shutting down
*///:~
