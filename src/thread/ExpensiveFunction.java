package thread;

import java.math.BigInteger;

/**
 * @author Mr.Sun
 * @date 2022年03月09日 21:59
 */
public class ExpensiveFunction implements Computable<String, BigInteger> {
    @Override
    public BigInteger compute(String arg) throws InterruptedException {
        // 在经过长时间的计算后
        return new BigInteger(arg);
    }
}
