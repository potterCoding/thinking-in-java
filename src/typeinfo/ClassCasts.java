package typeinfo;

/**
 * @author Mr.Sun
 * @date 2022年02月18日 21:26
 *
 * 新的转型语法
 */
class Building {}
class House extends Building {}

public class ClassCasts {
    public static void main(String[] args) {
        Building b = new House();
        Class<House> houseType = House.class;
        House h = houseType.cast(b);
        h = (House)b; // ... or just do this.
    }
}
