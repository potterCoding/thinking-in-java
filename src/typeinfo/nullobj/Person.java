package typeinfo.nullobj;

/**
 * @author Mr.Sun
 * @date 2022年02月21日 15:03
 *
 * 空对象定义
 */
public class Person {

    public final String first;
    public final String last;
    public final String address;
    // etc.
    public Person(String first, String last, String address){
        this.first = first;
        this.last = last;
        this.address = address;
    }

    public String toString() {
        return "Person: " + first + " " + last + " " + address;
    }

    public static class NullPerson extends Person implements Null {
        private NullPerson() { super("None", "None", "None"); }
        public String toString() { return "NullPerson"; }
    }

    public static final Person NULL = new NullPerson();
}
