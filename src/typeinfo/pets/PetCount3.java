package typeinfo.pets;

import utils.MapData;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Mr.Sun
 * @date 2022年02月20日 11:24
 *
 * 动态的instanceof
 * Class.isInstance()方法提供了一种动态地测试对象的途径
 */
public class PetCount3 {

    static class PetCounter extends LinkedHashMap<Class<? extends Pet>,Integer> {
        // PetCounter Map预加载了LiteralPetCreator.allTypes中的类型
        public PetCounter() {
            // MapData这个类接受一个Iterable和一个常数值（在本例中是0）
            // 然后用allTypes中的元素作为键，用0作为值，来填充Map
            super(MapData.map(LiteralPetCreator.allTypes, 0));
        }

        public void count(Pet pet) {
            // Class.isInstance() eliminates instanceofs:
            for(Map.Entry<Class<? extends Pet>,Integer> pair : entrySet())
                if(pair.getKey().isInstance(pet))
                    put(pair.getKey(), pair.getValue() + 1);
        }
        public String toString() {
            StringBuilder result = new StringBuilder("{");
            for(Map.Entry<Class<? extends Pet>,Integer> pair
                    : entrySet()) {
                result.append(pair.getKey().getSimpleName());
                result.append("=");
                result.append(pair.getValue());
                result.append(", ");
            }
            result.delete(result.length()-2, result.length());
            result.append("}");
            return result.toString();
        }
    }
    public static void main(String[] args) {
        PetCounter petCount = new PetCounter();
        for(Pet pet : Pets.createArray(20)) {
            System.out.print(pet.getClass().getSimpleName() + " ");
            petCount.count(pet);
        }
        System.out.println();
        System.out.println(petCount);
    }
}
