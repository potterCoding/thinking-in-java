package container.deep;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Mr.Sun
 * @date 2022年08月26日 21:01
 *
 * 填充容器工具类测试
 */
public class FillListTest {
    public static void main(String[] args) {
        List<StringAddress> list = new ArrayList<>(
                Collections.nCopies(4, new StringAddress("hello")));
        System.out.println(list);

        // fill()方法只能替换list中已经存在的元素，而不能添加新的元素
        Collections.fill(list, new StringAddress("world!"));
        System.out.println(list);
    }
}

class StringAddress {
    private String str;

    public StringAddress(String s) {
        this.str = s;
    }

    @Override
    public String toString() {
        return super.toString() + " " + str;
    }
}
